defmodule IntegerToEnglish do
  @moduledoc "Convert an integer into english words."

  use IntegerToEnglish.Macros

  @doc """
  Convert an integer into english words.

  ## Examples

      iex> IntegerToEnglish.integer_to_english(12)
      "twelve"
      iex> IntegerToEnglish.integer_to_english(-3034)
      "negative three thousand and thirty four"
      iex> IntegerToEnglish.integer_to_english(3823404)
      "three million, eight hundred twenty three thousand, four hundred four"

  """
  def integer_to_english(i) when i < 0, do: "negative " <> integer_to_english(i * -1)
  def integer_to_english(0), do: "zero"
  def integer_to_english(1), do: "one"
  def integer_to_english(2), do: "two"
  def integer_to_english(3), do: "three"
  def integer_to_english(4), do: "four"
  def integer_to_english(5), do: "five"
  def integer_to_english(6), do: "six"
  def integer_to_english(7), do: "seven"
  def integer_to_english(8), do: "eight"
  def integer_to_english(9), do: "nine"
  def integer_to_english(10), do: "ten"
  def integer_to_english(11), do: "eleven"
  def integer_to_english(12), do: "twelve"
  def integer_to_english(13), do: "thirteen"
  def integer_to_english(14), do: "fourteen"
  def integer_to_english(15), do: "fifteen"
  def integer_to_english(16), do: "sixteen"
  def integer_to_english(17), do: "seventeen"
  def integer_to_english(18), do: "eighteen"
  def integer_to_english(19), do: "nineteen"

  generate_clause("twenty", 20, 30)
  generate_clause("thirty", 30, 40)
  generate_clause("fourty", 40, 50)
  generate_clause("fifty", 50, 60)
  generate_clause("sixty", 60, 70)
  generate_clause("seventy", 70, 80)
  generate_clause("eighty", 80, 90)
  generate_clause("ninety", 90, 100)
  generate_clause("hundred", 100, 1000)
  generate_clause("thousand", 1000, 1_000_000)
  generate_clause("million", 1_000_000, 1_000_000_000)
  generate_clause("billion", 1_000_000_000, 1_000_000_000_000)
end
