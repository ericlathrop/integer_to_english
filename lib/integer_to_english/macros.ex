defmodule IntegerToEnglish.Macros do
  @moduledoc false

  defmacro __using__(_opts) do
    quote do
      import IntegerToEnglish.Macros

      defp prefix(i, dividend) when i >= 100 do
        IntegerToEnglish.integer_to_english(dividend) <> " "
      end

      defp prefix(_i, _dividend), do: ""

      defp remainder(_i, 0), do: ""

      defp remainder(i, rem) when i >= 1000 and rem < 100,
        do: " and " <> IntegerToEnglish.integer_to_english(rem)

      defp remainder(i, rem) when i >= 1000, do: ", " <> IntegerToEnglish.integer_to_english(rem)
      defp remainder(_i, rem), do: " " <> IntegerToEnglish.integer_to_english(rem)
    end
  end

  defmacro generate_clause(name, min, max) do
    quote do
      def integer_to_english(i) when is_integer(i) and i >= unquote(min) and i < unquote(max) do
        dividend = Integer.floor_div(i, unquote(min))
        rem = i - dividend * unquote(min)

        prefix(i, dividend) <>
          unquote(name) <> remainder(i, rem)
      end
    end
  end
end
