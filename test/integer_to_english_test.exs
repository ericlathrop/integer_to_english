defmodule IntegerToEnglishTest do
  use ExUnit.Case
  doctest IntegerToEnglish
  import IntegerToEnglish

  test "0" do
    assert integer_to_english(0) == "zero"
  end

  test "7" do
    assert integer_to_english(7) == "seven"
  end

  test "12" do
    assert integer_to_english(12) == "twelve"
  end

  test "100" do
    assert integer_to_english(100) == "one hundred"
  end

  test "300" do
    assert integer_to_english(300) == "three hundred"
  end

  test "328" do
    assert integer_to_english(328) == "three hundred twenty eight"
  end

  test "-328" do
    assert integer_to_english(-328) == "negative three hundred twenty eight"
  end

  test "1,000" do
    assert integer_to_english(1000) == "one thousand"
  end

  test "2,842" do
    assert integer_to_english(2842) == "two thousand, eight hundred fourty two"
  end

  test "2,842.1 raises" do
    assert_raise FunctionClauseError, fn ->
      integer_to_english(2842.1)
    end
  end

  test "-2,842.1 raises" do
    assert_raise FunctionClauseError, fn ->
      integer_to_english(-2842.1)
    end
  end

  test "7,012" do
    assert integer_to_english(7012) == "seven thousand and twelve"
  end

  test "839,254" do
    assert integer_to_english(839_254) ==
             "eight hundred thirty nine thousand, two hundred fifty four"
  end

  test "1,000,000" do
    assert integer_to_english(1_000_000) ==
             "one million"
  end

  test "1,000,001" do
    assert integer_to_english(1_000_001) ==
             "one million and one"
  end

  test "38,391,975" do
    assert integer_to_english(38_391_975) ==
             "thirty eight million, three hundred ninety one thousand, nine hundred seventy five"
  end

  test "23,789,204,789" do
    assert integer_to_english(23_789_204_789) ==
             "twenty three billion, seven hundred eighty nine million, two hundred four thousand, seven hundred eighty nine"
  end
end
