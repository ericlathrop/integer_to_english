defmodule IntegerToEnglish.MixProject do
  use Mix.Project

  def project do
    [
      app: :integer_to_english,
      deps: deps(),
      description: "Convert an integer into english words.",
      docs: [
        extras: ["LICENSE.md"]
      ],
      elixir: "~> 1.14",
      name: "IntegerToEnglish",
      package: package(),
      source_url: "https://gitlab.com/ericlathrop/integer_to_english",
      version: "0.1.0"
    ]
  end

  defp deps do
    [
      {:credo, "~> 1.7", only: [:dev, :test], runtime: false},
      {:ex_doc, "~> 0.27", only: :dev, runtime: false}
    ]
  end

  defp package() do
    [
      files: ~w(lib .formatter.exs mix.exs README* LICENSE*),
      licenses: ["MIT"],
      links: %{"GitLab" => "https://gitlab.com/ericlathrop/integer_to_english"}
    ]
  end
end
