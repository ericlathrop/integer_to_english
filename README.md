# IntegerToEnglish

[Hex](https://hex.pm/packages/integer_to_english)
[HexDocs](https://hexdocs.pm/integer_to_english)

Convert an integer into english words.

## Examples

```elixir
iex> IntegerToEnglish.integer_to_english(12)
"twelve"
iex> IntegerToEnglish.integer_to_english(-3034)
"negative three thousand and thirty four"
iex> IntegerToEnglish.integer_to_english(3823404)
"three million, eight hundred twenty three thousand, four hundred four"
```

## Installation

The package can be installed by adding `integer_to_english` to your list of
dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:integer_to_english, "~> 0.1.0"}
  ]
end
```
